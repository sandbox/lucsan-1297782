<?php

/**
 * @file
 * admin interface included functions
 */

/**
 * Implements hook_form().
 */
function modmaker_admin_form() {
  $default_module_save_folder = variable_get('modmaker_default_save_folder', '');
  $default_template_set_version = variable_get('modmaker_default_template_set_version', '7');
  $default_cache_flushing = variable_get('modmaker_cache_flushing', 0);
  $default_auto_install = variable_get('modmaker_auto_install', 0);
  $default_module_name = variable_get('modmaker_new_module_name', 'my_new_module');
  $default_module_description = variable_get('modmaker_new_module_description', 'Module description');
  $default_module_package = variable_get('modmaker_new_module_package', 'Development');

  $form['form_description'] = array(
    '#type' => 'item',
    '#description' => t('Note: all values are also set on submiting the Use form (ie: they are over-written when creating a new module).'),
  );

  $form['modmaker_new_module_name'] = array(
    '#type' => 'textfield',
    '#title' => t('module name'),
    '#default_value' => $default_module_name,
  );

  // The .info file details.
  $form['modmaker_new_module_description'] = array(
    '#type' => 'textfield',
    '#title' => t('module description'),
    '#default_value' => $default_module_description,
  );

  $form['modmaker_new_module_package'] = array(
    '#type' => 'textfield',
    '#title' => t('module package'),
    '#default_value' => $default_module_package,
  );

  $form['modmaker_default_save_folder'] = array(
    '#type' => 'textfield',
    '#title' => t('module location'),
    '#default_value' => $default_module_save_folder,
    '#description' => t('Choose a location for your module files.'),
  );

  $form['auto_install'] = array(
    '#type' => 'checkbox',
    '#title' => t('auto install module'),
    '#default_value' => $default_auto_install,
    '#description' => t("Automatically install the module once it's built."),
  );

  $form['clear_cache'] = array(
    '#type' => 'checkbox',
    '#title' => t('flushes all caches on build'),
    '#default_value' => $default_cache_flushing,
    '#description' => t("Automatically clear the caches once the module is built."),
  );

  return system_settings_form($form);
}
