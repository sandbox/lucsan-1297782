
-- SUMMARY --

Modmaker creates (and optionally installs) a working module with a selection of
working forms; simple, dynamic and ajax. Ready for a developer to begin writing
a module.

The new module is build from a set of scaffold templates which can be cloned and
edited as the user desires.

Simply pick the template set from the dropdown, add a couple of values such as a
name for the module, description and package.

Choose where you would like the module to be placed, and if you want it 
imediatly installed. Deselect any form types you dont want, and additional 
skeleton functions (ie: block, theme, menu, etc)

Click submit to create the new file set.

For a full description of the module, visit the project page:
  http://drupal.org/sandbox/lucsan/1297782

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/sandbox/issues/lucsan/1297782

  
-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

Modmaker exposes on the navigation menu and /modmaker.


-- CONFIGURATION --

* No configuration required. (You may wish to give certain roles permission)


-- CUSTOMIZATION --

Currently there are no customisation controls. Customisation of template files
is via direct file edit.


-- TROUBLESHOOTING --

Some options when not selected may cause the new module to error. Typically the
simple form, when not included in the module, will cause the tabs to throw up an
error. You will need to delete the refrence from the new module menu function.


-- FAQ --


-- CONTACT --

Current maintainers:
* Luke Tennant (lucsan) http://drupal.org/user/206004/

This project has been sposored by:
* Icepalms Limited
  Drupal consultancy and development.
