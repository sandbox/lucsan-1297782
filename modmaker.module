<?php
/**
 * @file
 * creates a skeleton module fileset
 *
 * @package modmaker
 * @version 1.4 Candidate
 */

/**
 * Implements hook_menu().
 */
function modmaker_menu() {
  // Admin menu items.
  $items['admin/config/development/modmaker'] = array(
    'title' => 'Modmaker',
    'description' => 'Use, clone and Administrate Modmaker.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('modmaker_form'),
    'access arguments' => array('access modmaker'),
    'type' => MENU_NORMAL_ITEM,
    // 'file' => 'modmaker.admin.inc',
  );
  $items['admin/config/development/modmaker/use'] = array(
    'title' => 'Use',
    'description' => 'Use modmaker to make a new module.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('modmaker_form'),
    'access arguments' => array('access modmaker'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 0,
  );

  $items['admin/config/development/modmaker/clone'] = array(
    'title' => 'Clone',
    'description' => 'Clone.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('modmaker_make_user_template_form'),
    'access arguments' => array('access modmaker'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
  );

  $items['admin/config/development/modmaker/admin'] = array(
    'title' => 'Admin',
    'description' => 'Clone.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('modmaker_admin_form'),
    'access arguments' => array('access modmaker'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'modmaker.admin.inc',
    'weight' => 2,
  );

  $items['modmaker'] = array(
    'title' => 'Modmaker',
    'description' => 'Create a module scaffold',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('modmaker_form'),
    'access arguments' => array('access modmaker'),
    'type' => MENU_NORMAL_ITEM,
    'weight' => 0,
  );

  $items['modmaker/modmaker_user_template'] = array(
    'title' => 'Modmaker template creator',
    'description' => 'Create a set of template to scaffold from',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('modmaker_make_user_template_form'),
    'access arguments' => array('access modmaker'),
    'type' => MENU_NORMAL_ITEM,
    'file' => '',
    'weight' => 0,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function modmaker_permission() {
  return array(
    'access modmaker' => array(
      'title' => t('modmaker access'),
      'description' => t('Allow users to edit modmaker'),
    ),
  );
}

/**
 * Implements hook_help().
 */
function modmaker_help($path, $arg) {
  if ($path == 'admin/help#modmaker') {
    return t('<h3>Modmaker</h3>
    Creates a set of module files with which you can quickly start customizing.
    <br />
    Uncheck sections you don\'t think you will want in your module.
    </br>
    The module files are placed by default in
    </br>
    <em>sites/default/modules/yourmodulename</em>.
    <br />
    </br>
    Begin editing the files by removing excess/unwanted code,
    which is there to remind or comments with additional information
    about the element.
    </br>
    On the whole an elelment will have many/all attributes,
    simply delete the ones you are not using.
    Some elements have their attributes commented out,
    these are generally the less frequently used attributes,
    and is a reminder of their existence and values.
    <br /><br />
    The template code should be seen as a complete listing of
    each element and the way to use it in Drupal.
    In other words, the output files are working api examples.');
  }
}

/**
 * Implements hook_form().
 *
 * @global stdClass $_modmaker_modmaker
 */
function modmaker_form($form_state) {
  global $_modmaker_modmaker;
  $_modmaker_modmaker = new stdClass();
  $_modmaker_modmaker->system_path = getcwd();
  $default_module_save_folder = variable_get('modmaker_default_save_folder', '');
  $default_template_set_version = variable_get('modmaker_default_template_set_version', '7');
  $default_cache_flushing = variable_get('modmaker_cache_flushing', 0);
  $default_auto_install = variable_get('modmaker_auto_install', 0);
  $default_module_name = variable_get('modmaker_new_module_name', 'my_new_module');
  $default_module_description = variable_get('modmaker_new_module_description', 'Module description');
  $default_module_package = variable_get('modmaker_new_module_package', 'Development');

  $module_save_folders = array(
    'sites/default/modules',
    'sites/default/modules/custom',
    'sites/all/modules',
    'sites/all/modules/custom',
  );
  // Check module folder permissions.
  $temp = array();
  foreach ($module_save_folders as $file_path) {
    if (is_writable(dirname($_modmaker_modmaker->system_path . '/' . $file_path))) {
      $temp[] = $file_path;
    }
  }
  $module_save_folders = $temp;

  // Menu and form editor.
  // Choose templates.
  $form['form_description'] = array(
    '#type' => 'item',
    '#description' => t('<h3>Module construction kit</h3>create a scaffold module from an existing template.'),
  );

  $form['version_mark'] = array(
    '#type' => 'select',
    '#title' => t('Template version to use'),
    '#options' => drupal_map_assoc(modmaker_identify_template_sets(array(".template.", ".module.api"))),
    '#default_value' => check_plain($default_template_set_version),
    '#description' => t('Choose a file set version to use.'),
    '#attributes' => array('onchange' => 'document.modmaker-form.submit();'),
  );

  $form['version_mark_submit'] = array(
    '#type' => 'submit',
    '#value' => t('version submit'),
    '#prefix' => '<div style="display:none;" >',
    '#suffix' => '</div>',
  );

  // Get the module sections to display on the form.
  $_modmaker_modmaker->module_sections = modmaker_get_template_sections('module', $default_template_set_version);

  $form['module_name'] = array(
    '#type' => 'textfield',
    '#title' => t('module name'),
    '#default_value' => $default_module_name,
  );

  // The .info file details.
  $form['module_description'] = array(
    '#type' => 'textfield',
    '#title' => t('module description'),
    '#default_value' => $default_module_description,
  );

  $form['module_package'] = array(
    '#type' => 'textfield',
    '#title' => t('module package'),
    '#default_value' => $default_module_package,
  );

  if (count($module_save_folders) < 1) {
    if ($default_module_save_folder == '') {
      $default_module_save_folder = 'sites/default/files';
    }
    $form['default_location'] = array(
      '#type' => 'textfield',
      '#title' => t('module location'),
      '#field_prefix' => "<div><b>Warning:</b> can not write to sites/all/modules or sites/default/modules." .
      "<p>You need to either set write permissions on one of these folders," .
      "<br />or select a writable folder to save the module files to," .
      "<br /> in which case, the new module will probably not auto install.</div>" .
      "{$_modmaker_modmaker->system_path}/",
      '#default_value' => $default_module_save_folder,
      '#description' => t('Choose a location for your module files.
      <em><smaller>(from your drupal root folder, no trailing slash.)<smaller></em>'),
    );
  }
  else {
    $form['default_location'] = array(
      '#type' => 'select',
      '#title' => t('module location'),
      '#options' => drupal_map_assoc($module_save_folders),
      '#default_value' => $default_module_save_folder,
      '#description' => t('Choose a location for your module files.'),
    );
  }

  $form['auto_install'] = array(
    '#type' => 'checkbox',
    '#title' => t('auto install module'),
    '#default_value' => $default_auto_install,
    '#description' => t("Automatically install the module once it's built."),
  );

  $form['clear_cache'] = array(
    '#type' => 'checkbox',
    '#title' => t('flushes all caches on build'),
    '#default_value' => $default_cache_flushing,
    '#description' => t("Automatically clear the caches once the module is built."),
  );

  // Module build choices (.install).
  $form['simple_table_text'] = array(
    '#markup' => '<label>' . t('.install file options') . '</label>',
  );

  $form['simple_table'] = array(
    '#type' => 'checkbox',
    '#title' => t('simple database table'),
    '#default_value' => 1,
  );

  // Module build choices (.module ).
  $form['module_options_text'] = array(
    '#markup' => '<label>' . t('.module file options') . '</label>',
  );

  foreach ($_modmaker_modmaker->module_sections as $value) {
    isset($value[1])? $description = $value[1] : $description = "";
    $form['section_' . $value[0]] = array(
      '#type' => 'checkbox',
      '#title' => check_plain(t("!section building skeleton.", array('!section' => $value[0]))),
      '#description' => check_plain(t("!description", array('!description' => $description))),
      '#default_value' => 1,
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('submit'),
  );

  return $form;
}

/**
 * Implements hook_form_submit().
 */
function modmaker_form_submit($form, &$form_state) {
  $form_values = $form_state['values'];

  $op = $form_values['op'];
  switch ($op) {
    case 'version submit':
      $version = check_plain($form_values['version_mark']);
      variable_set('modmaker_default_template_set_version', $version);
      $form_state['redirect'] = array(
        current_path(),
        array('query' => array()));
      return;
  }

  $mod_values = array();
  foreach ($form_values as $key => $value) {
    if ($key == 'clear_cache') {
      variable_set('modmaker_cache_flushing', $value);
    }
    if ($key == 'auto_install') {
      variable_set('modmaker_auto_install', $value);
    }
    if ($key == 'module_name') {
      variable_set('modmaker_new_module_name', $value);
    }
    if ($key == 'module_description') {
      variable_set('modmaker_new_module_description', $value);
    }
    if ($key == 'module_package') {
      variable_set('modmaker_new_module_package', $value);
    }
    $mod_values[$key] = $value;
  }

  // Create the module files.
  modmaker_make_mod($mod_values);

  if ($form_values['auto_install']) {
    include_once 'includes/module.inc';
    if (module_enable(array($mod_values['module_name']))) {
      drupal_set_message(check_plain(t("modmaker installed !module_name.", array('!module_name' => $mod_values['module_name']))));
    }
    else {
      drupal_set_message(check_plain(t("Modmaker was unable to install {$mod_values['module_name']}.
      This is most likely due to your new module being in a location
      Drupal does not recognise as a module folder. You may need to
      manually copy your new module to a valid location.", array('!module_name' => $mod_values['module_name']))), 'warning');
    }
  }

  // Clear all caches.
  if ($form_values['clear_cache']) {
    drupal_flush_all_caches();
    drupal_set_message(t('modmaker flushed all caches.'));
  }
}

/**
 * Makes the module file stack, called by module_form_submit.
 * @global stdClass $_modmaker_modmaker
 *
 * @param array $mod_values
 *   Takes $form_state['values]
 */
function modmaker_make_mod($mod_values) {
  global $_modmaker_modmaker;
  $_modmaker_modmaker->module_name = check_plain($mod_values['module_name']);
  $_modmaker_modmaker->version = check_plain($mod_values['version_mark']);
  $_modmaker_modmaker->directory = $mod_values['default_location'];

  // Set up new module directories in default/modules.
  modmaker_make_module_directory();
  modmaker_make_module_files($mod_values);
}

/**
 * Makes the module files.
 *
 * @param array $mod_values
 *   These are $form_state['values']
 */
function modmaker_make_module_files($mod_values) {
  global $_modmaker_modmaker;
  $module_name = $_modmaker_modmaker->module_name;
  $path = $_modmaker_modmaker->system_path . '/' . $_modmaker_modmaker->directory . '/';
  $module_path = $path . $module_name . '/';
  $version = $_modmaker_modmaker->version;

  $template_set_locations = modmaker_identify_template_sets(array('.template.', '.' . $version . '.'), FALSE);
  $template_set = array();
  foreach ($template_set_locations as $key => $value) {
    $tmp = explode('/', $value);
    $reference = $tmp[count($tmp) - 1];
    $reference = str_replace(array("modmaker.template.", $version . ".", ".api"), "", $reference);
    $reference = str_replace(array("."), "_", $reference);
    $files[] = $reference;
  }
  $found = FALSE;
  foreach ($files as $key => $value) {
    if ($value == 'module') {
      $found = TRUE; break;
    }
  }

  if (!$found) {
    drupal_set_message(check_plain(t('Template set must have a module.api file. None found for version !version', array('!version' => $version))));
    return;
  }

  foreach ($files as $file) {
    // This template has a file make function.
    if (function_exists('modmaker_make_template_' . $file . '_file')) {
      // Replaced by following method.
      $function = 'modmaker_make_template_' . $file . '_file';
      $return = $function($mod_values);
      $file = str_replace("_", ".", $file);
    }
    else {
      // No specific file make function, use generic.
      $file = str_replace("_", ".", $file);
      $return = modmaker_make_template_generic_file($file, $mod_values);
    }

    // Write out the new module file.
    $handle = fopen($module_path . $module_name . '.' . $file, 'w');
    fwrite($handle, $return);
    fclose($handle);
    drupal_set_message(check_plain(t('writen file !file_details', array('!file_details' => $module_path . $module_name . '.' . $file))));
  }
  // Write out the css and js files.
  // TODO: check if file exists to prevent error msg when overwriten.
  $handle = fopen($module_path . $module_name . '.css', 'w');
  fwrite($handle, "/* $module_name.css  */");
  fclose($handle);
  drupal_set_message(check_plain(t('writen file !file_details', array('!file_details' => $module_path . 'css/' . $module_name . '.css'))));

  $handle = fopen($module_path . $module_name . '.js', 'w');
  fwrite($handle, "/* $module_name.js   */\njQuery(document).ready(function () {  });");
  fclose($handle);
  drupal_set_message(check_plain(t('writen file !file_details', array('!file_details' => $module_path . 'js/' . $module_name . '.js'))));
}

/**
 * Template file processing function - info.
 *
 * @param array $mod_values
 *   These are $form_state['values'].
 *
 * @return string
 *   File contents.
 */
function modmaker_make_template_info_file($mod_values) {
  global $_modmaker_modmaker;
  $contents = modmaker_get_template_file('info', $mod_values['version_mark']);
  $contents = str_replace('$module_name', $_modmaker_modmaker->module_name, $contents);
  $contents = str_replace('$module_description', $mod_values['module_description'], $contents);
  $contents = str_replace('$module_package', $mod_values['module_package'], $contents);
  return $contents;
}

/**
 * Template file processing function - module.
 *
 * @param array $mod_values
 *   these $form_state['values'].
 *
 * @return string
 *   File contents.
 */
function modmaker_make_template_module_file($mod_values) {
  global $_modmaker_modmaker;
  $contents = modmaker_get_template_file('module', $mod_values['version_mark']);
  $contents = str_replace('$module_name', $_modmaker_modmaker->module_name, $contents);
  $contents = str_replace('$module_description', $mod_values['module_description'], $contents);

  // Go through each section and either remove the section or
  // remove the section reference {[section]}.
  foreach ($_modmaker_modmaker->module_sections as $section) {
    $include = $mod_values['section_' . $section[0]];

    if ($include) {
      // Check to see if a description is in the item block.
      $item = isset($section[1])? $section[0] . '|' . $section[1] : $section[0];
      $contents = str_replace(array('{[' . $item . ']}', '{[/' . $section[0] . ']}'), '', $contents);
      $contents = str_replace(array('{-[' . $section[0] . ']}', '{-[/' . $section[0] . ']}'), '', $contents);
    }
    else {
      $contents = modmaker_remove_unwanted_template_section($contents, $section[0]);
      $contents = modmaker_remove_unwanted_template_contingencies($contents, $section[0]);
    }
  }
  return $contents;
}

/**
 * Template file processing function - install.
 *
 * @param array $mod_values
 *   These are $form_state['values'].
 *
 * @return string
 *   File contents.
 */
function modmaker_make_template_install_file($mod_values) {
  global $_modmaker_modmaker;
  $contents = modmaker_get_template_file('install', $mod_values['version_mark']);
  $contents = str_replace('$module_name', $_modmaker_modmaker->module_name, $contents);
  return $contents;
}

/**
 * Template file processing function - admin.inc .
 *
 * @param array $mod_values
 *   These are $form_state['values'].
 *
 * @return string
 *   File contents.
 */
function modmaker_make_template_admin_inc_file($mod_values) {
  global $_modmaker_modmaker;
  $contents = modmaker_get_template_file('admin.inc', $mod_values['version_mark']);
  $contents = str_replace('$module_name', $_modmaker_modmaker->module_name, $contents);
  return $contents;
}

/**
 * Template file processing function - data.inc .
 *
 * @param array $mod_values
 *   These are $form_state['values'].
 *
 * @return string
 *   File contents.
 */
function modmaker_make_template_data_inc_file($mod_values) {
  global $_modmaker_modmaker;
  $contents = modmaker_get_template_file('data.inc', $mod_values['version_mark']);
  $contents = str_replace('$module_name', $_modmaker_modmaker->module_name, $contents);
  return $contents;
}

/**
 * Template file processing function - generic template file.
 *
 * @param string $file_reference
 *   A file path.
 * @param array $mod_values
 *   These are $form_state['values'].
 *
 * @return string
 *   File contents.
 */
function modmaker_make_template_generic_file($file_reference, $mod_values) {
  global $_modmaker_modmaker;
  $contents = modmaker_get_template_file($file_reference, $mod_values['version_mark']);
  $contents = str_replace('$module_name', $_modmaker_modmaker->module_name, $contents);
  $contents = str_replace('$module_description', $mod_values['module_description'], $contents);
  return $contents;
}

/**
 * Implements hook_form().
 */
function modmaker_make_user_template_form() {
  global $_modmaker_modmaker;
  $_modmaker_modmaker = new stdClass();
  $_modmaker_modmaker->system_path = getcwd();
  $existing_versions = modmaker_identify_template_sets(array(".template.", ".module.api"));

  $form['form_description'] = array(
    '#type' => 'item',
    '#description' => t('Scaffold construction kit - make your own template set from an existing template set.'),
  );

  $form['version']['base'] = array(
    '#type' => 'select',
    '#title' => t('Select Version to use as base'),
    '#description' => t('Base the new template set on an existing version set.'),
    '#options' => drupal_map_assoc($existing_versions),
    '#default_value' => t('7'),
  );

  $form['version']['user'] = array(
    '#type' => 'textfield',
    '#title' => t('New version name'),
    '#description' => t('A name for the version ie: user_1, user.1.0, 66, 7.22. No spaces leading or trailing dots.'),
    '#default_value' => t('user.1.0'),
    '#required' => TRUE,
  );

  $form['create'] = array(
    '#type' => 'submit',
    '#value' => t('Create'),
  );

  return $form;
}

/**
 * Creates template files from existing files. Implements hook_form_submit().
 */
function modmaker_make_user_template_form_submit($form, &$form_state) {
  $base_version = $form_state['values']['base'];
  $user_version = $form_state['values']['user'];

  // Get selected base version template files.
  $base_version_files = modmaker_identify_template_sets(array(".template.", $base_version), FALSE);
  $versions = array();
  foreach ($base_version_files as $key => $value) {
    $user_filename = str_replace("." . $base_version . ".", "." . $user_version . ".", $value);

    $contents = '';
    $base_handle = fopen($value, "r");
    $contents = fread($base_handle, filesize($value));
    fclose($base_handle);

    // Create new version template files.
    $user_handle = fopen($user_filename, 'w');
    if ($user_handle) {
      fwrite($user_handle, $contents);
      fclose($user_handle);
      drupal_set_message(check_plain(t('Created - !file', array('!file' => $user_filename))));
    }
    else {
      drupal_set_message(check_plain(t("Unable to write !file_name</br>
      Modmaker is unable to write to it's own folder.
      This is most likely due to a write permissions issue.
      ", array('!file_name' => $user_filename))));
    }

  }
}

/**
 * Makes the module directories if needed.
 *
 * @global stdClass $_modmaker_modmaker
 */
function modmaker_make_module_directory() {
  global $_modmaker_modmaker;
  $module_name = $_modmaker_modmaker->module_name;
  $system_path = $_modmaker_modmaker->system_path;
  $directory = $_modmaker_modmaker->directory;

  $folders = explode("/", $directory);

  $seek_location = '/';
  for ($i = 0; $i < (count($folders) - 1); $i++) {
    $seek_location .= $folders[$i] . '/';
  }
  $check_folder = $folders[count($folders) - 1];

  // Scan for default/modules folder, if it doesn't exist, create it.
  // TODO: scan other popular module locations.
  if (!modmaker_scan_for_directory($seek_location, $check_folder)) {
    mkdir($system_path . $seek_location . $check_folder);
    drupal_set_message(check_plain(t('directory created !file_details', array('!file_details' => $seek_location . $check_folder))));
  }

  // Scan for target module folder, if it doesn't exist, create it.
  if (!modmaker_scan_for_directory($seek_location . $check_folder . '/', $module_name)) {
    $mkdir_path = $system_path . $seek_location . $check_folder . '/' . $module_name;
    mkdir($mkdir_path);
    drupal_set_message(check_plain(t('directory created !directory', array('!directory' => $mkdir_path))));
    // Create module subfolders.
    // TODO: make the folder creation interface configurable.
  }
}

/**
 * Scans for a directory and checks if it exists.
 *
 * @global stdClass $_modmaker_modmaker
 *
 * @param string $path
 *   A path.
 * @param string $directory
 *   A directory name.
 *
 * @return bool
 *   TRUE if template directory is found.
 */
function modmaker_scan_for_directory($path, $directory) {
  global $_modmaker_modmaker;
  $scan = scandir($_modmaker_modmaker->system_path . $path);
  foreach ($scan as $key => $value) {
    if (is_dir($_modmaker_modmaker->system_path . $path . $value) && $value != '.' && $value != '..') {
      if ($value == $directory) {
        return TRUE;
      }
    }
  }
  return FALSE;
}

/**
 * Scans a directory for files.
 *
 * @global stdClass $_modmaker_modmaker
 *
 * @param string $path
 *   A path.
 *
 * @return array
 *   List of file paths.
 */
function modmaker_scan_files($path) {
  global $_modmaker_modmaker;
  $files = array();
  $scan = scandir($_modmaker_modmaker->system_path . $path);

  foreach ($scan as $key => $value) {
    if (is_file($_modmaker_modmaker->system_path . $path . $value)) {
      $files[] = $_modmaker_modmaker->system_path . $path . $value;
    }
  }
  return $files;
}

/**
 * Identifies template sets.
 *
 * @param array $identifiers
 *   Optional (eg: .template).
 * @param array $return_versions
 *   Optional (eg: 7, base.1).
 *
 * @returns array
 *   Array of string values.
 */
function modmaker_identify_template_sets($identifiers = array(), $return_versions = TRUE) {
  $files = modmaker_scan_files('/' . drupal_get_path('module', 'modmaker') . '/scaffolds/');
  $templates = array();
  foreach ($files as $key => $value) {
    $include = 0;
    foreach ($identifiers as $identifier) {
      if (stripos($value, $identifier) !== FALSE) {
        $include++;
      }
      else {
        break;
      }
    }
    if ($include == count($identifiers)) {
      $templates[] = $value;
    }
  }

  if ($return_versions) {
    $existing_versions = modmaker_extract_versions_from_filenames($templates);
    return $existing_versions;
  }
  else {
    // Template filenames.
    return $templates;
  }
}

/**
 * Extracts version identifiers from filenames.
 *
 * @param array $filenames
 *   Expects an array of template.x.module.api files.
 *
 * @return array
 *   Of versions.
 */
function modmaker_extract_versions_from_filenames($filenames = array()) {
  $versions = array();
  foreach ($filenames as $key => $value) {
    $first = stripos($value, ".template.");
    $last = stripos($value, ".module.");
    $version = substr($value, $first + 10, $last - $first - 10);
    $versions[] = $version;
  }
  return $versions;
}

/**
 * Reads a template file looking for the section {[ identifier ]}.
 *
 * @param string $template
 *   Template type eg: module, install.
 * @param int $version
 *   File set version number.
 *
 * @return array
 *   A keyed array of key = section name, value = section description.
 */
function modmaker_get_template_sections($template = 'module', $version = 7) {
  $contents = modmaker_get_template_file($template, $version);

  // Extract main sections {[section]}.
  $sections = array();
  $open = stripos($contents, '{[');
  $close = stripos($contents, ']}', $open);
  $sections[] = explode("|", substr($contents, $open + 2, $close - $open - 2));
  $open = stripos($contents, '{[/', $close);
  $close = stripos($contents, ']}', $open);

  while ($open = stripos($contents, '{[', $close)) {
    $close = stripos($contents, ']}', $open);
    $sections[] = explode("|", substr($contents, $open + 2, $close - $open - 2));
    $open = stripos($contents, '{[/', $close);
    $close = stripos($contents, ']}', $open);
  }
  return $sections;
}

/**
 * Generic template loader.
 *
 * @global stdClass $_modmaker_modmaker
 *
 * @param string $file
 *   A file name.
 * @param int $version
 *   Drupal version number.
 *
 * @return string
 *   File contents.
 */
function modmaker_get_template_file($file, $version = 7) {
  global $_modmaker_modmaker;
  $system_path = $_modmaker_modmaker->system_path;
  $filename = $system_path . '/' . drupal_get_path('module', 'modmaker') . '/scaffolds/modmaker.template.' . $version . '.' . $file . '.api';
  $handle = fopen($filename, "r");
  $contents = fread($handle, filesize($filename));
  fclose($handle);
  return $contents;
}

/**
 * Strips unwanted sections from the module template file.
 *
 * @param string $contents
 *   File contents.
 * @param string $section
 *   Section identifier (ie: {[menu]} ).
 *
 * @return string
 *   File contents.
 */
function modmaker_remove_unwanted_template_section($contents, $section) {
  $start = stripos($contents, '{[' . $section);
  $end = stripos($contents, '{[/' . $section . ']}');
  $i = strlen($section) - 0 + 5;
  $new_contents = substr($contents, 0, $start);
  $new_contents .= substr($contents, ($end + $i), strlen($contents));
  return $new_contents;
}

/**
 * Strips unwanted contingent sections from the module template file.
 *
 * @param string $contents
 *   File contents.
 * @param string $section
 *   Section identifier (ie: {[menu]} ).
 *
 * @return string
 *   File contents.
 */
function modmaker_remove_unwanted_template_contingencies($contents, $section) {
  $start = stripos($contents, '{-[' . $section);
  $end = stripos($contents, '{-[/' . $section . ']}');
  $i = strlen($section) - 0 + 6;

  // No contingent sections exists.
  if ($start === FALSE) {
    return $contents;
  }
  $new_contents = substr($contents, 0, $start);
  $new_contents .= substr($contents, ($end + $i), strlen($contents));
  return $new_contents;
}
